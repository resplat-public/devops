# devops

This repo contains all public keys and pages for RCS DevOps team. All assets in `public/` is automatically deployed, please DO NOT put private keys in this repo.